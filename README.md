---
title: Onion Reveal
subtitle: A static site generator using reveal.js with a Tor theme
author: Community Team - The Tor Project
---

## Onion Reveal

This is a [reveal.js][] slides compiler similar to [Onion TeX Slim][].

It was written during a [Tor Hackweek project][].

[reveal.js]: https://revealjs.com

[Onion TeX Slim]: https://gitlab.torproject.org/rhatto/onion-tex-slim

[Tor Hackweek project]: https://gitlab.torproject.org/tpo/community/hackweek/-/issues/15

## Features

* Builds web-based slideshows with Tor theming.
* Modular and non-intrusive: can be included within any project.
* Localization support through [gettext][].

[gettext]: https://www.gnu.org/software/gettext/manual/html_node/PO-Files.html

## How it works

This software could works like this:

* Uses a Makefile/script target that traverses a folder looking for `.md` files.
* Then it builds the HTML slides using a [Pandoc][] [reveal.js][] template.
* With CI/CD for automation, in a way that's easy to integrate into existing projects.

[Pandoc]: https://pandoc.org

## Examples

* [Onion Reveal's website](https://tpo.pages.torproject.net/web/onion-reveal/).
* [Sample presentation](examples/sample.md).

## Repository

Onion Reveal's repository is hosted at [https://gitlab.torproject.org/tpo/web/onion-reveal/](https://gitlab.torproject.org/tpo/web/onion-reveal/).

## Requirements

Onion Reveal requires the following software:

* [Git](https://git-scm.com).
* [NPM](https://www.npmjs.com).
* [Gulp](https://gulpjs.com).
* [Pandoc](https://pandoc.org).
* [rsync](https://rsync.samba.org).

An example installation script tested on [Debian][] Bookworm is available at [scripts/onion-reveal-provision-build][].

[Debian]: https://www.debian.org
[texlive-latex-extra]: https://tracker.debian.org/pkg/texlive-extra
[scripts/onion-reveal-provision-build]: scripts/onion-reveal-provision-build
[po4a]: https://po4a.org

## Optional dependencies

* [GNU Make](https://www.gnu.org/software/make) for building through Makefiles.
* [po4a][] to handle translations.

## Installation

This repository is ready to be used in any existing repository.
It can be installed in a number of ways as follows.

## Forking the project

Simply fork this project and change whatever you need.

## As a Git submodule

You can simply put it as a Git submodule somewhere, like in a `vendors` folder of your project:

    mkdir -p vendor && \
    git submodule add --depth 1 \
      https://gitlab.torproject.org/tpo/web/onion-reveal vendors/onion-reveal

You also need to initialize Onion Reveal's submodules:

    git -C vendors/onion-reveal submodule update --init --depth 1

Then use symbolic links to reference all the needed files from the root of your repository.

## Manually copying

Even simpler, copy all the relevant files from this repository into your project.

## Outside the project

The Onion Reveal repository can be installed anywhere in your system.
In fact, it does not need to be stored inside your project if you don't want to.
You just need to make sure to invoke the scripts from your project folder or pass the appropriate folder or file path as arguments to it's scripts.

## Other ways

* `git-subtree(1)`.
* ?

## Installation notes

* If you plan to use either GitLab CI/CD or GitHub Actions to build your documentation, make sure to at least copy/adapt the corresponding files from the submodule.
* You can't just symlink those since the submodule won't be accessible when GitLab and GitHub looks for CI/CD or Actions files.
* The provided CI configuration takes care of initializing the submodule during build time, but they should be available in the main repo in order to do so.

## Project layout

Example project layout:

    * /path/to/my/project
        * slides
          * README.md
          * slide-for-conference-1
              * slide-for-conference-1.md
          * slide-for-conference-2
              * slide-for-conference-2.md
        * vendors
            * onion-reveal

## Building the theme

To build the theme, follow the instructions at [vendors/onion-reveal/vendors/revealjs_tor_theme/README][] or use the following script, if you're using a [Debian][]-like system:

    ./vendors/onion-reveal/scripts/onion-reveal-build-theme

[vendors/onion-reveal/vendors/revealjs_tor_theme/README]:

## Compiling all slides

Onion Reveal can be used to build static websites from any set of Markdown files it finds, so it's recommended to run it against your project slides folder.

Once all dependencies are installed and the required files are in place in your repository, just run the build script:

    vendors/onion-reveal/scripts/onion-reveal-build-all slides/

If invoked without arguments, the script above will build slides from all Markdown file it finds recursively from the current folder.

## Compiling a single slide

You can also build a single slide deck:

    vendors/onion-reveal/scripts/onion-reveal-build \
      slides/slide-for-conference-1/slide-for-conference-1.md

## Translation support

Onion Reveal plays well with localization (l10n).
Out of the box, it supports translations through [L10n for Markdown][].
If you have [po4a][] installed, you can use it by copying/customizing the provided [l10n.sample][] and running the following script everytime you want to sync your translations:

    ./vendors/l10n-for-markdown/scripts/l10n-for-markdown-update-locales

[L10n for Markdown]: https://gitlab.torproject.org/tpo/community/l10n-for-markdown/
[l10n.sample]: https://gitlab.torproject.org/tpo/community/l10n-for-markdown/-/blob/main/l10n.sample

## Makefile

Onion Reveal also comes with a [sample Makefile][] that can be customized to your needs, placed in the desired folder and then used to build all your slides through

    make onion-reveal-build-all

You can also build specific slides using `make`:

    make -C slides slides/slide-for-conference-1/slide-for-conference-1.html

[sample Makefile]: https://gitlab.torproject.org/tpo/community/onion-reveal/-/blob/main/Makefile.onion-reveal

## Continuous Integration

Onion Reveal  support Continuous Integration (CI) through GitLab.
Check the [.gitlab-ci-onion-reveal.yml][] file for details.

[.gitlab-ci-onion-reveal.yml]: .gitlab-ci-onion-reveal.yml

## TODO and issues

Check existing and report new issues in the [ticket queue][].

[ticket queue]: https://gitlab.torproject.org/tpo/web/onion-reveal/-/issues
