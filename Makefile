#
# This Makefile just includes any Makefile.* available in the current folder.
#
# It can be as an example to keep your projects own Makefile separate from
# Onion Reveal Makefile.
#

# Process any other Makefile whose filename matches Makefile.*
# See https://www.gnu.org/software/make/manual/html_node/Include.html
#
# Some of those files might even contain local customizations/overrides
# that can be .gitignore'd, like a Makefile.local for example.
-include Makefile.*
